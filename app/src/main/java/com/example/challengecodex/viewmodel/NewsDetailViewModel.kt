package com.example.challengecodex.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengecodex.model.CommentResponse
import com.example.challengecodex.model.NewsResponse
import com.example.challengecodex.networking.NewsRepository

class NewsDetailViewModel : ViewModel() {

    private var commentList: MutableLiveData<ArrayList<CommentResponse>>? = MutableLiveData()
    private var newsRepository: NewsRepository? = NewsRepository()
    private var newsResponse: MutableLiveData<NewsResponse>? = MutableLiveData()
    private var currentFavorit: String? = null
    private var favorit = false

    fun init() {
        newsResponse?.value?.kids?.let {
            commentList = newsRepository?.getComment(it)
        }
    }

    fun getCommentList(): MutableLiveData<ArrayList<CommentResponse>>? {
        return commentList
    }

    fun setNewsResponse(newsResponse: NewsResponse) {
        this.newsResponse?.value = newsResponse
    }

    fun getNewsResponse(): MutableLiveData<NewsResponse>? {
        return newsResponse
    }

    fun setFavorit(favorit: Boolean) {
        this.favorit = favorit
    }

    fun getFavorit(): Boolean {
        return favorit
    }

    fun setCurrentFavorit(currentFavorit: String?) {
        this.currentFavorit = currentFavorit
    }

    fun getCurrentFavorit(): String? {
        return currentFavorit
    }
}