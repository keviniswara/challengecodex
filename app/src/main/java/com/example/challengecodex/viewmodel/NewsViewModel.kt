package com.example.challengecodex.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengecodex.model.NewsResponse
import com.example.challengecodex.networking.NewsRepository

class NewsViewModel : ViewModel() {

    private var newsList: MutableLiveData<ArrayList<NewsResponse>>? = MutableLiveData()
    private var newsRepository: NewsRepository? = NewsRepository()
    private var progress: MutableLiveData<Int>? = newsRepository?.newsProgress
    private var maxNewsCount = newsRepository?.maxNewsCount

    fun init() {
        newsList = newsRepository?.getNews()
    }

    fun getNewsRepository(): MutableLiveData<ArrayList<NewsResponse>>? {
        return newsList
    }

    fun getProgress(): MutableLiveData<Int>? {
        return progress
    }

    fun getMaxNewsCount(): MutableLiveData<Int>? {
        return maxNewsCount
    }
}