package com.example.challengecodex.networking

import androidx.lifecycle.MutableLiveData
import com.example.challengecodex.model.CommentResponse
import com.example.challengecodex.model.NewsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsRepository {
    private var newsApi = RetrofitService.cteateService(NewsApi::class.java)
    var newsProgress: MutableLiveData<Int> = MutableLiveData()
    var maxNewsCount: MutableLiveData<Int> = MutableLiveData()

    fun getNews() : MutableLiveData<ArrayList<NewsResponse>> {
        var newsData: MutableLiveData<ArrayList<NewsResponse>> = MutableLiveData()

        newsData.value = ArrayList()

        newsApi.getTopStoriesList()?.enqueue(object: Callback<ArrayList<String>?> {
            override fun onFailure(call: Call<ArrayList<String>?>?, t: Throwable?) {
                newsData.value = null
            }

            override fun onResponse(call: Call<ArrayList<String>?>?,
                                    response: Response<ArrayList<String>?>?) {
                response?.isSuccessful?.let {
                    response.body()?.let {
                        maxNewsCount.value = 21
                        for (i in 0..20) {
                            newsApi.getNews(it.get(i))?.enqueue(object: Callback<NewsResponse?> {
                                override fun onFailure(call: Call<NewsResponse?>, t: Throwable) {
                                    newsData.value = null
                                    newsProgress.value = (newsProgress.value?: 0) + 1
                                }

                                override fun onResponse(call: Call<NewsResponse?>,
                                                        response: Response<NewsResponse?>) {
                                    if (response.isSuccessful) {
                                        response.body()?.run {
                                            newsData.value?.add(this)
                                            newsData.value = newsData.value
                                        }
                                    }
                                    newsProgress.value = (newsProgress.value?: 0) + 1
                                }
                            })
                        }
                    }
                }
            }
        })
        return newsData
    }

    fun getComment(id: List<String>): MutableLiveData<ArrayList<CommentResponse>> {
        var commentData: MutableLiveData<ArrayList<CommentResponse>> = MutableLiveData()

        commentData.value = ArrayList()

        if (id.isNotEmpty()) {
            for (item in id) {
                newsApi.getComment(item)?.enqueue(object : Callback<CommentResponse?> {
                    override fun onFailure(call: Call<CommentResponse?>, t: Throwable) {
                        commentData.value = null
                    }

                    override fun onResponse(call: Call<CommentResponse?>,
                                            response: Response<CommentResponse?>) {
                        if (response.isSuccessful) {
                            response.body()?.run {
                                commentData.value?.add(this)
                                commentData.value = commentData.value
                            }
                        }
                    }
                })
            }
        }
        return commentData
    }
}