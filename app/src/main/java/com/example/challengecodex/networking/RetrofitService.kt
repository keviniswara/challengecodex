package com.example.challengecodex.networking

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitService {
    companion object {
        private val retrofit = Retrofit.Builder()
            .baseUrl("https://hacker-news.firebaseio.com/v0/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        fun <S> cteateService(serviceClass: Class<S>?): S {
            return retrofit.create(serviceClass)
        }
    }
}