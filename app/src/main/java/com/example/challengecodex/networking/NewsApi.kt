package com.example.challengecodex.networking

import androidx.lifecycle.MutableLiveData
import com.example.challengecodex.model.CommentResponse
import com.example.challengecodex.model.NewsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface NewsApi {
    @GET("topstories.json")
    fun getTopStoriesList(): Call<ArrayList<String>?>?

    @GET("item/{id}.json")
    fun getNews(@Path(value = "id", encoded = true) itemId: String): Call<NewsResponse?>?

    @GET("item/{id}.json")
    fun getComment(@Path(value = "id", encoded = true) itemId: String): Call<CommentResponse?>?
}