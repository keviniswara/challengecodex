package com.example.challengecodex.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.challengecodex.R
import com.example.challengecodex.databinding.ItemNewsBinding
import com.example.challengecodex.model.NewsResponse

class NewsAdapter(private val items: MutableList<NewsResponse>) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    private lateinit var binding: ItemNewsBinding
    var onItemClick: ((NewsResponse) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(
            inflater, R.layout.item_news, parent, false)
        return NewsViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    inner class NewsViewHolder(var binding:ItemNewsBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onItemClick?.invoke(items[adapterPosition])
            }
        }

        fun bind(item: NewsResponse) {
            binding.tvTitle.text = item.title
            binding.tvComment.text = "Jumlah Komentar: ${item.kids?.size ?: 0}"
            binding.tvScore.text = "Score: ${item.score}"
        }
    }
}