package com.example.challengecodex.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.challengecodex.R
import com.example.challengecodex.databinding.ItemNewsdetailBinding
import com.example.challengecodex.model.CommentResponse

class CommentAdapter(private val items: MutableList<CommentResponse>)
    : RecyclerView.Adapter<CommentAdapter.CommentViewHolder>() {

    private lateinit var binding: ItemNewsdetailBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(
            inflater, R.layout.item_newsdetail, parent, false
        )
        return CommentViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    inner class CommentViewHolder(var binding: ItemNewsdetailBinding) : RecyclerView.ViewHolder(binding.root) {

        private var text : String? = null
//        private var full : Boolean = false

        fun bind(item: CommentResponse) {
            binding.tvCommentby.text = item.by
            text = item.text
            binding.tvCommenttext.text = text

            //this code is for limiting comment length
            /*text?.let {
                if (it.length > 100) {
                    full = false
                    binding.tvCommenttext.text = "${it.slice(0..30)}.."
                } else {
                    full = true
                    binding.tvCommenttext.text = it
                }
            }
            binding.root.setOnClickListener {
                text?.let {
                    if (it.length > 100) {
                        if (full) {
                            full = false
                            binding.tvCommenttext.text = "${it.slice(0..30)}.."
                        } else {
                            full = true
                            binding.tvCommenttext.text = it
                        }
                    }
                }
            }*/
        }
    }
}