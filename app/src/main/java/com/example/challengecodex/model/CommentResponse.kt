package com.example.challengecodex.model

import android.os.Parcel
import android.os.Parcelable

class CommentResponse(val by: String? = null, val id: String? = null,
                      val kids: List<String>? = null, val parent: String? = null,
                      val time: Int? = null, val text: String? = null,
                      val type: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.createStringArrayList(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(by)
        parcel.writeString(id)
        parcel.writeStringList(kids)
        parcel.writeString(parent)
        parcel.writeValue(time)
        parcel.writeString(text)
        parcel.writeString(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CommentResponse> {
        override fun createFromParcel(parcel: Parcel): CommentResponse {
            return CommentResponse(parcel)
        }

        override fun newArray(size: Int): Array<CommentResponse?> {
            return arrayOfNulls(size)
        }
    }
}