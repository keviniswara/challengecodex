package com.example.challengecodex.model

import android.os.Parcel
import android.os.Parcelable

class NewsResponse(val by: String? = null, val id: String? = null,
                   val kids: List<String>? = null, val score: Int? = null,
                   val time: Int? = null, val title: String? = null, val type: String? = null,
                   val url: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.createStringArrayList(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(by)
        parcel.writeString(id)
        parcel.writeStringList(kids)
        parcel.writeValue(score)
        parcel.writeValue(time)
        parcel.writeString(title)
        parcel.writeString(type)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewsResponse> {
        override fun createFromParcel(parcel: Parcel): NewsResponse {
            return NewsResponse(parcel)
        }

        override fun newArray(size: Int): Array<NewsResponse?> {
            return arrayOfNulls(size)
        }
    }
}