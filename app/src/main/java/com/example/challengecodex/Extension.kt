package com.example.challengecodex

import java.text.SimpleDateFormat
import java.util.*


class Extension() {
    companion object {
        fun create(): Extension = Extension()
    }

    // To get date in "dd/mm/yyyy" format from millis
    fun getDate(milliSeconds: Long): String? {

        val formatter = SimpleDateFormat("dd/mm/yyyy")
        val calendar: Calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }
}