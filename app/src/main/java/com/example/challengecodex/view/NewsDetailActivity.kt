package com.example.challengecodex.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.challengecodex.Extension
import com.example.challengecodex.R
import com.example.challengecodex.adapter.CommentAdapter
import com.example.challengecodex.databinding.ActivityNewsdetailBinding
import com.example.challengecodex.model.CommentResponse
import com.example.challengecodex.model.NewsResponse
import com.example.challengecodex.viewmodel.NewsDetailViewModel

class NewsDetailActivity : AppCompatActivity() {

    private var commentArrayList: ArrayList<CommentResponse> = ArrayList()
    private lateinit var commentAdapter: CommentAdapter
    private lateinit var binding: ActivityNewsdetailBinding
    private lateinit var rvNews: RecyclerView
    private lateinit var newsViewModel: NewsDetailViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        newsViewModel = ViewModelProviders.of(this).get(NewsDetailViewModel::class.java)
        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_newsdetail)

        rvNews = binding.rvComment

        newsViewModel.setNewsResponse(intent.getParcelableExtra("newsresponse") ?: NewsResponse())
        newsViewModel.setCurrentFavorit(intent.getStringExtra("favorit"))
        newsViewModel.init()

        newsViewModel.getNewsResponse()?.observe(this, Observer { data ->
            data?.let {
                binding.tvTitle.text = it.title
                binding.tvBy.text = "By ${it.by}"
                binding.tvDate.text = Extension.create().getDate(it.time?.toLong() ?: 0)
                binding.tvDesc.text = it.url
            }
        })

        newsViewModel.getCommentList()?.observe(this, Observer { data ->
            if (data != null) {
                var news: ArrayList<CommentResponse> = data
                commentArrayList.clear()
                commentArrayList.addAll(news)
                commentAdapter.notifyDataSetChanged()
                if (data.size != 0) {
                    binding.tvCommentTitle.visibility = View.VISIBLE
                    binding.rvComment.visibility = View.VISIBLE
                }
            }
        })
        initFavorit()
        setupCommentRecyclerView()
    }

    fun initFavorit() {
        newsViewModel.getCurrentFavorit()?.let {
            if (it.isNotBlank()) {
                if (it == newsViewModel.getNewsResponse()?.value?.title) {
                    newsViewModel.setFavorit(true)
                    binding.ivGreystar.visibility = View.VISIBLE
                    binding.ivYellowstar.visibility = View.GONE
                }
            }
        }
        binding.ivGreystar.setOnClickListener {
            newsViewModel.setFavorit(true)
            binding.ivGreystar.visibility = View.INVISIBLE
            binding.ivYellowstar.visibility = View.VISIBLE
        }

        binding.ivYellowstar.setOnClickListener {
            newsViewModel.setFavorit(false)
            binding.ivGreystar.visibility = View.VISIBLE
            binding.ivYellowstar.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, NewsDetailActivity::class.java)
        if (newsViewModel.getFavorit()) {
            intent.putExtra("favoritnews", newsViewModel.getNewsResponse()?.value?.title)
        } else {
            newsViewModel.getCurrentFavorit()?.apply {
                if (this != newsViewModel.getNewsResponse()?.value?.title) {
                    intent.putExtra("favoritnews", this)
                } else intent.putExtra("favoritnews", "")
            }
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun setupCommentRecyclerView() {
        commentAdapter = CommentAdapter(commentArrayList)
        rvNews.layoutManager = LinearLayoutManager(this)
        rvNews.adapter = commentAdapter
    }
}