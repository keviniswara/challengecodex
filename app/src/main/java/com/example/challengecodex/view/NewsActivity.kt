package com.example.challengecodex.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.challengecodex.R
import com.example.challengecodex.adapter.NewsAdapter
import com.example.challengecodex.databinding.ActivityNewsBinding
import com.example.challengecodex.model.NewsResponse
import com.example.challengecodex.viewmodel.NewsViewModel

class NewsActivity : AppCompatActivity() {

    private var newsArrayList: ArrayList<NewsResponse> = ArrayList()
    private lateinit var newsAdapter: NewsAdapter
    private lateinit var binding: ActivityNewsBinding
    private lateinit var rvNews: RecyclerView
    private lateinit var newsViewModel: NewsViewModel

    companion object {
        val FAVORITE_NEWS = 1000
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_news)

        rvNews = binding.rvNews
        newsViewModel = ViewModelProviders.of(this).get(NewsViewModel::class.java)
        newsViewModel.init()

        newsViewModel.getNewsRepository()?.observe(this, Observer { data ->
            if (data != null) {
                var news: ArrayList<NewsResponse> = data
                newsArrayList.clear()
                newsArrayList.addAll(news)
                newsAdapter.notifyDataSetChanged()
            }
        })
        setupRecyclerView()

        newsViewModel.getMaxNewsCount()?.observe(this, Observer { max ->
            binding.pbHorizontal.max = max
        })

        newsViewModel.getProgress()?.observe(this, Observer {progress ->
            if (progress == binding.pbHorizontal.max) {
                binding.pbHorizontal.visibility = View.GONE
            } else {
                binding.pbHorizontal.visibility = View.VISIBLE
            }
            binding.pbHorizontal.progress = progress
        })
    }

    private fun setupRecyclerView() {
        newsAdapter = NewsAdapter(newsArrayList)
        newsAdapter.onItemClick = { news ->
            val intent = Intent(this, NewsDetailActivity::class.java).apply {
                putExtra("newsresponse", news)
                putExtra("favorit", binding.tvFavorit.text)
            }
            startActivityForResult(intent, FAVORITE_NEWS)
        }
        rvNews.layoutManager = GridLayoutManager(this, 2)
        rvNews.adapter = newsAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == FAVORITE_NEWS && resultCode == Activity.RESULT_OK) {
            val favoritNews: String? = data?.getStringExtra("favoritnews")
            if (favoritNews == null) {
                binding.tvFavorit.visibility = View.GONE
                binding.tvFavorit.text = ""
            } else if (favoritNews == "") {
                binding.tvFavorit.visibility = View.GONE
                binding.tvFavorit.text = ""
            } else {
                binding.tvFavorit.visibility = View.VISIBLE
                binding.tvFavorit.text = favoritNews
            }
        } else {
            binding.tvFavorit.visibility = View.GONE
            binding.tvFavorit.text = ""
        }
    }
}
